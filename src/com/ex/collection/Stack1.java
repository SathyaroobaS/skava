package com.ex.collection;
import java.util.*;  
public class Stack1{  
public static void main(String args[]){  
	
Stack<String> stack = new Stack<String>();  

stack.push("Ayush");  
stack.push("Garvit");  
stack.push("Amit");  
stack.push("Ashish");  
stack.push("Garima");  
stack.pop();  

System.out.println(stack);

System.out.println("Stack size is " + stack.size());

// check if stack is empty
if (stack.empty())
	System.out.println("Stack is Empty");
else
	System.out.println("Stack is not Empty");
}  
}