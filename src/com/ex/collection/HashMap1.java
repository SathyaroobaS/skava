package com.ex.collection;
import java.util.*;  
class HashMap1{  
 public static void main(String args[]){  
	 HashMap<Integer,String> map=new HashMap<Integer,String>();          
     map.put(100,"Amit");    
     map.put(101,"Vijay");    
     map.put(102,"Rahul");  
     map.put(103, "Gaurav");  
   System.out.println("Initial list of elements: "+map);  
   
   map.remove(100);  
   System.out.println("Updated list of elements: "+map);  
   
   map.remove(101);  
   System.out.println("Updated list of elements: "+map);  
   
   map.remove(102, "Rahul");  
   System.out.println("Updated list of elements: "+map);
   
   map.replace(103, "Sathya");  
   System.out.println("Updated list of elements: "+map);
   
   map.put(104,null);  
   System.out.println("Updated list of elements: "+map);
   
   map.put(null,null);  
   System.out.println("Updated list of elements: "+map);
   
   map.put(null,null);  
   System.out.println("Updated list of elements: "+map);
   
   map.put(103, "Sathya");  
   System.out.println("Updated list of elements: "+map);

 }  
}
