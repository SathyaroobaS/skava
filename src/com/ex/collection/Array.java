package com.ex.collection;
import java.util.*;  
import java.util.Collections;
class Array{  
	public static void main(String args[]){  
		
		ArrayList<String> list=new ArrayList<String>();
		
		list.add("Hello"); 
		list.add("Hi");  
		list.add("Welcome");  
		list.add("Thankyou"); 
		
		//System.out.println(list.get(0));
		list.set(1, "Changed");
		
		list.remove(0);
		System.out.println(list);  
		
		System.out.println(list.size());
	    
		Collections.sort(list); 
	    
		System.out.println(list);  
		
		list.clear();
		
		System.out.println(list);  
		
		System.out.println(list.size());
 
		}

	}  
