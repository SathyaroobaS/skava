package com.ex.exception;
import java.util.Scanner;

class InvalidAgeException extends Exception{  
	 InvalidAgeException(String s){  
	  super(s);  
	 }  
	}  

public class Customexception 
{
	static void checkAge(int age)throws InvalidAgeException
	{
		if(age<18)
		{
			throw new InvalidAgeException("You are not eligible for voting"); 
		}
		else 
		{
		      System.out.println("You are eligible for voting");

		}
		
		
	}

	public static void main(String[] args) 
	{
		 Scanner sc = new Scanner(System.in);
		 int age=sc.nextInt();
		 try
			{
	     	checkAge(age);
			}
		 catch(Exception e) {
			 System.out.println("Exception occured...");
		 }
		 finally
		 {
			 System.out.println("end of the block");
			 
		 }
		 

	}

}
