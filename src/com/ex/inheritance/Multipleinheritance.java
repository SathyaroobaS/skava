package com.ex.inheritance;

interface one 
{ 
	public void print_hi(); 
} 

interface two 
{ 
	public void print_hello(); 
} 

interface three extends one,two 
{ 
	public void print_hi(); 
} 
class child implements three 
{ 
	@Override
	public void print_hi() { 
		System.out.println("hi"); 
	} 

	public void print_hello() 
	{ 
		System.out.println("hello"); 
	} 
} 
