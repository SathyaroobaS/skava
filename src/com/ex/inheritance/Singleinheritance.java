package com.ex.inheritance;
class Shape
{
	public String colour;
	public Shape(String colour)
	{
		this.colour=colour;
	}
	public String toString()
	{
		return(" the colour is "+colour);
	}
	
}
class Circle extends Shape
{
	public int radius;
	public Circle(String colour,int radius)
	{
		super(colour);
		this.radius=radius;
		
	}
	@Override
	public String toString()
	{
		return("The shape is Circle,its radius is "+radius+"cm "+"and"+super.toString());
	}
}

