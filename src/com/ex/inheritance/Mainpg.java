package com.ex.inheritance;
import java.util.Scanner;
public class Mainpg {
	public static void single()
	{
	Circle c1 = new Circle("violet",5);
    System.out.println(c1.toString());
	}
	public static void multi()
	{
		child c = new child(); 
		c.print_hi(); 
		c.print_hello(); 
		c.print_hi();
	}
	public static void multilevel()
	{
		Cylinder c2 = new Cylinder("violet",5,8);
        System.out.println(c2.toString()); 	
	}
	public static void hierar()
	{
	
		Circle c1 = new Circle("violet",5);
		System.out.println(c1.toString()); 
		Square s1=new Square("Blue",10);
		System.out.println(s1.toString()); 
	}


	public static void main(String[] args) 
	{
		Scanner s2 = new Scanner(System.in);
		 int choice=s2.nextInt();
	        switch (choice) { 
	        case 1: 
	        	single();
	            break; 
	        case 2: 
	        	multi();
	            break; 
	        case 3: 
	        	multilevel();
	            break; 
	        case 4: 
	        	hierar();
	            break; 
	        }

	}

}
