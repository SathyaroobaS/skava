package com.ex.inheritance;
class Square extends Shape
{
	public int side;
	public Square(String colour,int side)
	{
		super(colour);
		this.side=side;
	}
	@Override
	public String toString()
	{
		return("The shape is Square,its side is "+side+"cm "+"and"+super.toString());
	}

}
