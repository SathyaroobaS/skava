package com.ex.inheritance;

class Cylinder extends Circle
{
	public int height;
	public Cylinder(String colour,int radius,int height)
	{
		super(colour,radius);
		this.height=height;
		
	}
	@Override
	public String toString()
	{
		return("the shape is cylinder,its radius is"+radius+"cm "+"the colour is "+colour+" its height is "+height);
	}

}

